package com.zuitt.s10act;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController

public class S10actApplication {

	public static void main(String[] args) {

		SpringApplication.run(S10actApplication.class, args);
	}
	@GetMapping("/posts")
	public String getPosts(){
		return "All posts retrieved.";
	}

	@PostMapping("/posts")
	public String createPost(){
		return "New post created.";
	}

	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post " + postid;
	}

	@DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + " has been deleted.";
	}

	@PutMapping("/posts/{postid}")
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	@GetMapping("/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user ){
		return "Posts for " + user + " have been retrieved";
	}

//Activity s10

	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}

	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "Viewing details of user " + userid;
	}

	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid, @RequestHeader(value = "Authorization") String user){
		if(user.isEmpty()) {
			return "Unauthorized access.";
		}
		return "The user " + userid + " has been deleted.";
	}
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}

}


